#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#     Luis Cañas-Díaz <lcanas@bitergia.com>
#

import codecs
import os.path
import re

from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))
readme_md = os.path.join(here, 'README.md')
version_py = os.path.join(here, 'e2edatacheck', '_version.py')

# Pypi wants the description to be in reStrcuturedText, but
# we have it in Markdown. So, let's convert formats.
# Set up thinkgs so that if pypandoc is not installed, it
# just issues a warning.
try:
    import pypandoc
    long_description = pypandoc.convert(readme_md, 'rst')
except (IOError, ImportError):
    print("Warning: pypandoc module not found, or pandoc not installed. "
          "Using md instead of rst")
    with codecs.open(readme_md, encoding='utf-8') as f:
        long_description = f.read()

with codecs.open(version_py, 'r', encoding='utf-8') as fd:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                        fd.read(), re.MULTILINE).group(1)


setup(name="e2edatacheck",
      description="End to end data checks for Grimoirelab indexes",
      long_description=long_description,
      url="https://gitlab.com/bitergia/devops/e2edatacheck",
      version=version,
      author="Bitergia",
      author_email="lcanas@bitergia.com",
      license="GPLv3",
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Intended Audience :: Developers',
          'Topic :: Software Development',
          'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
          'Programming Language :: Python :: 3'
      ],
      keywords="development repositories analytics git github gitlab check grimoirelab",
      packages=[
          'e2edatacheck',
          'e2edatacheck.ds',
          'e2edatacheck.db',
          'e2edatacheck.uiparser'
      ],
      setup_requires=[
          'wheel',
          'pandoc'
      ],
      install_requires=[
          'python-dateutil>=2.8.0',
          'requests>=2.21.0',
          'beautifulsoup4>=4.7.1',
          'certifi>=2019.3.9',
          'colorama>=0.4.1',
          'elasticsearch==6.3.1',
          'elasticsearch-dsl==6.3.1',
          'prettytable>=0.7.2',
          'urllib3==1.24.1'
      ],
      scripts=[
          'bin/e2edatacheck'
      ],
      zip_safe=False)
