# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Luis Cañas-Díaz <lcanas@bitergia.com>
#   Quan Zhou <quan@bitergia.com>
#

import logging

from .db.api import count_docs_per_bucket, count_docs
from .uiparser.github import (items_per_repo as github_items_per_repo)
from .uiparser.gitlab import (items_per_repo as gitlab_items_per_repo,
                              ISSUE_CATEGORY as GITLAB_ISSUE_CATEGORY,
                              MR_CATEGORY as GITLAB_MR_CATEGORY)
from .db.database import Database


logger = logging.getLogger(__name__)


def count_stored_items_per_bucket(host, myindex, origin, field, insecure):
    """
    Count items stored in a ES cluster for an specific origin and creating buckets. The output is a dictionary with
    the bucket names and the total count

    :param host: ES endpoint
    :param myindex: index name
    :param origin: value of the origin field to be used for the search
    :param field: field to be use to create buckets
    :param insecure: allow insecure server connections when using SSL
    :return:
    """
    db = Database(host, insecure)
    conn = db.connect()

    result = count_docs_per_bucket(conn, myindex, origin, field)
    output = {}
    for entry in result:
        output[entry['key']] = entry['doc_count']

    return output


def count_stored_items(host, myindex, origin, insecure):
    """
    Count items stored in a ES cluster for an specific origin.
    The output is the total count.

    :param host: ES endpoint
    :param myindex: index name
    :param origin: value of the origin field to be used for the search
    :param insecure: allow insecure server connections when using SSL
    :return: total count number
    """
    db = Database(host, insecure)
    conn = db.connect()

    result = count_docs(conn, myindex, origin)
    return result
