# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Luis Cañas-Díaz <lcanas@bitergia.com>
#

import logging

logger = logging.getLogger(__name__)


class BaseError(Exception):
    """Base class error.

    Derived classes can overwrite error message declaring 'message' property.
    """
    message = "Unknown error"

    def __init__(self, **kwargs):
        super(BaseError, self).__init__(kwargs)
        self.msg = self.message % kwargs

    def __str__(self):
        return self.msg

class ESConnectionError(BaseError):
    """Exception raised when the Elasticsearch connection does not work."""

    message = "Failed to establish a connection with %(cause)s, it is alive?"

class InvalidJsonFile(BaseError):
    """Exception raised when the JSON is not valid"""

    message = "Invalid JSON file"

class ErrorGettingUpstreamData(BaseError):
    """Exception raised when an error was found parsing the user inferface"""

    message = "It was not possible to get the data from the upstream user interface"