# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Luis Cañas-Díaz <lcanas@bitergia.com>
#

import requests

from bs4 import BeautifulSoup

from ..exceptions import ErrorGettingUpstreamData

ISSUE_CATEGORY='issue'
MR_CATEGORY='merge_request'


def items_per_repo(repo_url, category):
    """Returns number of items per state found in the web interface of Gitlab issues
    or merge requests. For a repo_url such as "https://gitlab.com/gitlab-org/gitlab-ce"
    it would get the list of items per state for either merge requests ( at
    https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/) or issues ( at
    https://gitlab.com/gitlab-org/gitlab-ce/issues)

    :param repo_url: URL of the repo to be queried
    :param category: name of the category to be queried
    :return: list of integers
    """
    repo_url = repo_url.replace('%2F', '/')

    url = _compose_url_for(repo_url, category)
    r = requests.get(url)

    if r.status_code != 200:
        raise ErrorGettingUpstreamData

    soup = BeautifulSoup(r.text, 'html.parser')
    pills = [i.get_text() for i in soup.find_all("span",
                                                 "badge badge-muted badge-pill gl-badge gl-tab-counter-badge sm")]
    int_pills = [int(j.replace(',', '')) for j in pills]

    return int_pills

def _compose_url_for(repo_url, category):
    if category == ISSUE_CATEGORY:
        return repo_url + "/issues"
    elif category == MR_CATEGORY:
        return repo_url + "/merge_requests"
