# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Luis Cañas-Díaz <lcanas@bitergia.com>
#   Quan Zhou <quan@bitergia.com>
#

import time

from . import api
from .exceptions import ErrorGettingUpstreamData


class DataSource():
    """Abstract class
    """
    def __init__(self, endpoint_raw, raw_index, endpoint_enriched, enriched_index, origin, insecure):
        self.endpoint_raw = endpoint_raw
        self.index_raw = raw_index
        self.endpoint_enriched = endpoint_enriched
        self.index_enriched = enriched_index
        self.origin = origin
        self.insecure = insecure
        self.diff_items = {}
        self._raw_items = {}
        self._upstream_items = {}
        self._enriched_items = {}

    @property
    def raw_items(self):
        if self._raw_items:
            return self._raw_items
        else:
            return self.query_raw_items()

    @property
    def enriched_items(self):
        if self._enriched_items:
            return self._enriched_items
        else:
            return self.query_enriched_items()

    @property
    def upstream_items(self):
        if self._upstream_items:
            return self._upstream_items
        else:
            return self.query_upstream_items()

    @property
    def reference_items(self):
        """Returns the set of items to be used as reference to calculate the error. Not all the data
        sources will have upstream items because it is not always possible to get the value we need
        from and UI (i.e. Git). In those cases the data source will overwrite this method to mark
        which one is the data set used as reference
        """
        return self.upstream_items

    def query_raw_items(self):
        raise NotImplementedError

    def query_enriched_items(self):
        raise NotImplementedError

    def query_upstream_items(self):
        raise NotImplementedError

    def _terms(self):
        raise NotImplementedError

    def _calculate_total(self, mydict):
        total = 0
        for k in mydict:
            total += mydict[k]
        return total

    def compare(self):
        """Calculates the diff between the enriched items and the items each data source uses as reference. It
        returns a dictionary with all the values
        """
        def _calculate_diff(dict_a, dict_b):
            output = {}
            for k in dict_a.keys():
                try:
                    dict_b_k_value = dict_b[k]
                except KeyError:
                    dict_b_k_value = 0
                output[k] = dict_a[k] - dict_b_k_value
            return output

        output = {'timestamp': time.time(),
                  'origin': self.origin,
                  'raw': self.raw_items,
                  'enriched': self.enriched_items,
                  'datasource': self.__class__.__name__}

        try:
            output["upstream"] = self.upstream_items
        except ErrorGettingUpstreamData:
            raise ErrorGettingUpstreamData
        except NotImplementedError:
            pass

        self.diff_items = _calculate_diff(self.enriched_items, self.reference_items)
        output['diff'] = self.diff_items

        return output


class DataSourceWithStatuses(DataSource):
    """Class inherited by data sources which have statuses in the items. These data sources are
    Github, Gitlab issues, etc ...
    """

    def query_raw_items(self):
        term = self._terms_for_raw()
        items = api.count_stored_items_per_bucket(self.endpoint_raw, self.index_raw,
                                                  self.origin, term, self.insecure)
        for k in items:
            self._raw_items[k] = items[k]
        self._raw_items['total'] = self._calculate_total(self._raw_items)
        return self._raw_items

    def query_enriched_items(self):
        term = self._terms_for_enriched()
        items = api.count_stored_items_per_bucket(self.endpoint_enriched, self.index_enriched,
                                                  self.origin, term, self.insecure)
        for k in items:
            self._enriched_items[k] = items[k]
        self._enriched_items['total'] = self._calculate_total(self._enriched_items)
        return self._enriched_items

    def _terms_for_raw(self):
        return 'data.state'

    def _terms_for_enriched(self):
        return 'state'
