# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Luis Cañas-Díaz <lcanas@bitergia.com>
#

from .. import api
from ..datasource import DataSourceWithStatuses


class Github(DataSourceWithStatuses): #FIXME Gitlab should be renamed to ... ?

    def query_upstream_items(self):
        u_values = api.github_items_per_repo(self.origin)
        self._upstream_items['open'] = u_values[0]
        self._upstream_items['closed'] = u_values[1]
        self._upstream_items['total'] = self._calculate_total(self._upstream_items)
        return self._upstream_items

