# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Luis Cañas-Díaz <lcanas@bitergia.com>
#   Quan Zhou <quan@bitergia.com>
#

from .. import api
from ..datasource import DataSource


class Git(DataSource):

    @property
    def reference_items(self):
        return self.raw_items

    def query_raw_items(self):
        self._raw_items['total'] = api.count_stored_items(self.endpoint_raw, self.index_raw,
                                                          self.origin, self.insecure)
        return self._raw_items

    def query_enriched_items(self):
        self._enriched_items['total'] = api.count_stored_items(self.endpoint_enriched, self.index_enriched,
                                                               self.origin, self.insecure)
        return self._enriched_items
