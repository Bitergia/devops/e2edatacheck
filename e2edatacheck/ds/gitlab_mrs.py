# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2019 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Luis Cañas-Díaz <lcanas@bitergia.com>
#

from .. import api
from ..datasource import DataSourceWithStatuses


class GitlabMRs(DataSourceWithStatuses):

    def query_upstream_items(self):
        u_values = api.gitlab_items_per_repo(self.origin, api.GITLAB_MR_CATEGORY)
        if not u_values:
            self._upstream_items['opened'] = 0
            self._upstream_items['merged'] = 0
            self._upstream_items['closed'] = 0
            self._upstream_items['total'] = 0
        else:
            self._upstream_items['opened'] = u_values[0]
            self._upstream_items['merged'] = u_values[1]
            self._upstream_items['closed'] = u_values[2]
            self._upstream_items['total'] = u_values[3]
        return self._upstream_items
