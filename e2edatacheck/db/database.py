# -*- coding: utf-8 -*-
#
# Copyright (C) 2015-2020 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA.
#
# Authors:
#   Luis Cañas-Díaz <lcanas@bitergia.com>
#   Quan Zhou <quan@bitergia.com>
#

import certifi
import elasticsearch as es
import logging
import urllib3
import warnings

from e2edatacheck.exceptions import ESConnectionError


urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
warnings.filterwarnings("ignore")
logger = logging.getLogger(__name__)


class Database:

    def __init__(self, endpoint, insecure):
        self._endpoint = endpoint
        self.insecure = insecure
        if 'biterg.io/data' in endpoint:
            self.insecure = False

    def connect(self):
        if self.insecure:
            client = es.Elasticsearch(self._endpoint, timeout=60, use_ssl=True, verify_certs=False,
                                      connection_class=es.RequestsHttpConnection)
        else:
            client = es.Elasticsearch(self._endpoint, timeout=60, use_ssl=True, ca_certs=certifi.where())
        if not client.ping():
            raise ESConnectionError(cause=self._endpoint)
        return client
